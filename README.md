ankrobes-server
===============

`ankrobes-server` is a wrapper around [pg-ankisyncd](https://gitlab.com/transcrobes/pg-ankisyncd) for use with [Transcrobes](https://gitlab.com/transcrobes). It's main purpose in life is to add support for using a Django database as the UserManager and to host the dockerfile and CI for integration into Transcrobes. See [the Transcrobes site](https://transcrob.es) for more information, particularly regarding the role of Anki (and so this Anki-compatible sync server) in the system.

Unless you intend to set up a Transcrobes cluster or want to add an Anki-compatible server to your existing Django site, you are probably better off looking at `pg-ankisyncd` or [ankisyncd](https://github.com/tsudoko/anki-sync-server).

Installation
============
```
$ pip install -r requirements.txt  # requirements.dev.txt for running the tests
```
You need a postgres server and user with permissions to create schemas and tables in the DB, and Django authentication.

The repository includes *4* levels of sub-modules :-). `ankrobes-server` includes `pg-ankisyncd`, which includes `ankisyncd`, which includes `anki`. There is no obligation to clone these repositories as sub-modules, but the `PYTHONPATH` must include all of them. Currently none of these projects are available on Pypi, hence them being included as submodules for easy deployment.

Running `ankrobes-server`
======================

`ankrobes-server` is intended to be run as part the [Kubernetes Helm chart for Transcrobes](https://gitlab.com/transcrobes/charts). `pg-ankisyncd` (and its parent, [ankisyncd](https://github.com/tsudoko/anki-sync-server) allows for configuration of allvalues via environment variables. Both have the possibility to configure via ini-style conf files, but the env vars will override anything set in the files. See those projects for more information on how this is achieved.

The `runserver.sh` script included in this repo exports default environment variables for rarely changed items, and includes commented examples for those values that are setup-dependent, such as DB connection settings, the collection path, etc. These commented values should all be provided to the container as environment values, or `runserver.sh` should be modified to include them.

The `runserver.sh` script also exports a `PYTHONPATH` with the 4 levels of submodules (see above) included. If you decide to put these projects elsewhere, then you will need to ensure that the `PYTHONPATH` includes the correct paths.

To run, simply execute:
```
$ ./runserver.sh
```
See the Dockerfile and requirements.txt for other dependencies.
