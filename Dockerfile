# vim:set ft=dockerfile:
FROM python:3.6-slim

RUN apt update && apt install -y portaudio19-dev gcc sqlite3

WORKDIR /app

COPY . /app

RUN pip install -r requirements.txt

EXPOSE 27701

CMD ["/bin/bash", "/app/runserver.sh"]
