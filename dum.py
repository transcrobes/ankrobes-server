from ankisyncd.users import SimpleUserManager

import django
from django.conf import settings
import django.contrib.auth

class DjangoUserManager(SimpleUserManager):
    """Authenticate against an existing Django database."""

    def __init__(self, config):
        self._conf = config
        SimpleUserManager.__init__(self, config["data_root"])

        db = {
            'default': {
                'ENGINE': 'django.db.backends.postgresql',
                'NAME': config["db_name"],
                'USER': config["db_user"],
                'PASSWORD': config["db_password"],
                'HOST': config["db_host"],
                'PORT': config["db_port"],
            }
        }
        auth_apps = [ 'django.contrib.auth', 'django.contrib.contenttypes', ]
        middleware = [
            'django.middleware.security.SecurityMiddleware',
            'django.middleware.common.CommonMiddleware',
            'django.contrib.auth.middleware.AuthenticationMiddleware',
        ]

        settings.configure(DATABASES=db, MIDDLEWARE=middleware, INSTALLED_APPS=auth_apps)
        django.setup()

    def authenticate(self, username, password):
        """
        Returns True if this username is allowed to connect with this password.
        False otherwise.
        """
        user = django.contrib.auth.authenticate(username=username, password=password)
        return bool(user)  # None if auth fails

