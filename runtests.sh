#!/bin/bash

export PYTHONPATH=$PYTHONPATH:pg-ankisyncd:pg-ankisyncd/asd:pg-ankisyncd/asd/ankisyncd/anki-bundled

# Standard values
export ANKISYNCD_BASE_URL=/sync/
export ANKISYNCD_BASE_MEDIA_URL=/msync/
export ANKISYNCD_PERSISTENCE_MANAGER=panki.PostgresPersistenceManager
export ANKISYNCD_SESSION_MANAGER=panki.PostgresSessionManager
export ANKISYNCD_USER_MANAGER=dum.DjangoUserManager
export ANKISYNCD_COLLECTION_WRAPPER=panki.PostgresCollectionWrapper

export ANKISYNCD_AUTH_DB_PATH=
export ANKISYNCD_SESSION_DB_PATH=

# Initialise to the Transcrobes-required collection setup
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export ANKISYNCD_COLLECTION_INIT=$DIR/sql/ankrobes.sql

# All these values are passed via Kubernetes/Helm
# ANKISYNCD_DB_HOST=127.0.0.1
# ANKISYNCD_DB_PORT=5432
# ANKISYNCD_DB_NAME=your_db_name
# ANKISYNCD_DB_USER=your_db_user
# ANKISYNCD_DB_PASSWORD=your_db_password
# ANKISYNCD_HOST=127.0.0.1
# ANKISYNCD_PORT=27701
# ANKISYNCD_DATA_ROOT=./collections

# FIXME: For the moment just running the ankisyncd tests
python -m unittest discover -v pg-ankisyncd/asd/tests
